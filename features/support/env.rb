require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'rspec/expectations'
require 'json'
require_relative '../../app'

RACK_ENV='test'
BASE_URL = 'http://localhost:4567'
if ENV['BASE_URL']
  BASE_URL = ENV['BASE_URL']
else
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end