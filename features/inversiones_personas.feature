#language: es
Característica: Cartera de inversiones de persona fisica

  Antecedentes:
    Dado que mi cuit es "20112223336"

  @wip
  Esquema del escenario: Inversion solo en dolares
    Dado que compro $ <capital> dólares a cotizacion $ <cotizacion_que_compre>
    Cuando vendo esos dolares a cotización $ <cotizacion_que_vendi>
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

  Ejemplos:
    | capital | cotizacion_que_compre | cotizacion_que_vendi | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |    1000 |        80.5           |      90.0            |       9500.0   |       95.0           |    9405.0    |
    |    1000 |        100.0          |     100.0            |            0   |        0             |         0    |
    |    1000 |        100.0          |      90.0            |     -10000.0   |        0             |  -10000.0    |
    |   10000 |        100.0          |     200.0            |    1000000.0   |        40000.0       |  960000.0    |

  @wip
  Esquema del escenario: Inversion solo en plazo fijo
    Dado que el interes de plazo fijo es <interes> % anual
    Cuando invierto $ <capital> en un plazo fijo a <plazo> dias
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>
  Ejemplos:
    | interes | capital | plazo | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |   10    |   1000  | 365   |     100.0      |         0.0          |     100.0    |
    |   10    |   1000  | 30    |       8.33     |         0.0          |       8.33   |

  @wip
  Escenario: Inversion solo en acciones
    Dado que compro $ 1000.0 en acciones a $ 50.0 por acción
    Cuando vendo esas acciones a $ 100.0 por acción
    Entonces obtengo una ganancia bruta de $ 1000.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ 990.0

  @wip
  Escenario: Inversion solo en acciones con perdida
    Dado que compro $ 1000.0 en acciones a $ 50.0 por acción
    Cuando vendo esas acciones a $ 40.0 por acción
    Entonces obtengo una ganancia bruta de $ -200.0
    Y $ 10.0 de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ -210.0


